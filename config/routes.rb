Rails.application.routes.draw do
  devise_for :users, :controllers => {:registrations => "registrations",  confirmations: 'confirmations'}
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root 'welcome#index'
  get 'load_cities' => 'welcome#load_cities'
  get 'load_state' => 'welcome#load_state'

  resources :bsp_trainings
  resources :llc_services
  resources :medications
  resources :medication_counts
  resources :mars
  resources :behaviors

  resources :users do
    get 'settings'
    get 'edit_password', on: :collection
    put 'update_password', on: :collection
    post 'add_individual', on: :collection
    put 'update_individual', on: :member
  end
end
