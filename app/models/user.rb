class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,:confirmable

  belongs_to :company, optional: true
  has_many :bsp_trainings
  has_many :llc_services
  has_many :medications
  has_many :medication_counts
  has_many :mars
  has_many :behaviors


  def full_name
    if first_name && last_name
      first_name + ' ' + last_name
      else
        first_name ? first_name : last_name
    end
  end
end
