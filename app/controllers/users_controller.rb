class UsersController < ApplicationController
  before_action :authenticate_user!

  def new
    @user = User.new
  end

  def add_individual
    @user = current_company.users.new(user_params)
    random_number = rand.to_s[2..11]
    @user.password = random_number
    @user.password_confirmation = random_number
    @user.confirmed_at = Time.now
    if @user.save
      UserMailer.welcome_email(@user).deliver
      flash[:notice] = "Indviduals is successfully created"
      redirect_to root_path
    else
      flash[:alert] = "Something worng please try again"
      render action: :new
    end
  end

  def edit
    @user = User.find_by_id(params[:id])
  end

  def update_individual
    @user = User.find_by_id(params[:id])
    if @user.update_attributes(user_params)
      flash[:notice] = "Indviduals is successfully Saved"
      redirect_to root_path
    else
      flash[:alert] = "Something worng please try again"
      render action: :edit
    end
  end



  def index
    @users = current_company.individuals
    if params[:search].present?
      search =  params[:search]
      @users = @users.where("first_name like ? or last_name like ? or state like ? or email like ? or medicare_number like ? or medicaid_number like ?",
                            "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%" , "%#{search}%" , "%#{search}%")
    end
  end

  def settings

  end

  def edit_password
    @user = current_user
  end

  def update_password
    @user = current_user
    if @user.update(user_params)
      # Sign in the user by passing validation in case their password changed
      bypass_sign_in(@user)
        flash[:notice] = "Password Successfully Updated"
        redirect_to root_path
    else
      flash[:alert] = "Something worng please try again"
      render "edit_password"
    end
  end

  private

  def user_params
    params.require(:user).permit!
  end
end
