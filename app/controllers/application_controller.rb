class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session
  helper_method :flash_types
  # before_action :authenticate_user!
  helper_method :current_company


  def current_company
    current_user.company if current_user.present?
  end

  def flash_types
    ['info', 'success', 'danger', 'warning', 'alert', 'error']
  end
end
